package personal.learn.maven;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Check "Info Template/Info/html/Index.html"
 */
public class Main {

    public static void main(String[] args) {
        Main.call1();
    }

    private static void call1() {
        Main.test1();
    }

    private static void test1() {
        Type type1 = new Type(1, "one", "The first");
        Type type2 = new Type(2, "two", "The second");
        Type type3 = new Type(3, "three", "The third");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            String teyp1Text = objectMapper.writeValueAsString(type1);
            String teyp2Text = objectMapper.writeValueAsString(type2);
            String teyp3Text = objectMapper.writeValueAsString(type3);
            System.out.println("Shaded");
            System.out.println(teyp1Text);
            System.out.println(teyp2Text);
            System.out.println(teyp3Text);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


}