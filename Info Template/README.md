# README #

This is a simple package named Info that should be included within projects in order to explain things using simple html/css and some code syntax highlighting using Highlight.js

The [file "Info Template/Info/html/Index.html"](https://bitbucket.org/lesu/info-template/src/4d159f6948c453e535e3aad75d46af111e95ba83/Info/html/Index.html?at=master&fileviewer=file-view-default) is the file that should be edited to include the data and description.